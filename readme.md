# Deploy Application from Jenkins to EC2 with Docker
CD - Deploy Application from Jenkins Pipeline to EC2 Instance (automatically with docker)

## Technologies Used
- AWS
- Jenkins
- Docker
- Linux
- Git
- Java
- Maven
- Docker Hub

## Project Description
- Prepare AWS EC2 Instance for deployment (Install Docker)
- Create SSH key credentials for EC2 server on Jenkins
- Extend the previous CI pipeline with deploy step to SSH into the remote EC2 instance and deploy newly built image from Jenkins server
- Configure security group on EC2 Instance to allow access to our
web application

## Prerequisites
- An AWS account
- Docker Account
- Linux machine configured with Docker and Git
- Private Jenkins server (from previous courses)

## Guide Steps
### Pre-Req Repository
- Initialize a new repository for a multibranch pipeline to utilize the files in [this repository](https://gitlab.com/twn-devops-bootcamp/latest/09-aws/java-maven-app/-/tree/starting-code).
- Create a new Jenkins multibranch pipeline with this repository. You can clone an existing that is configured. If you need to know how to create a multibranch pipeline, you can use [this how-to](https://gitlab.com/devops-public-projects/m8p-full-jenkins-ci-pipeline-nodejs).
- We will call the new pipeline `aws-temp-app-code` for this project.
### Install SSH Plugin in Jenkins and Configure SSH Credential Type
- Jenkins > Manage Jenkins > Plugins > Available Plugins > **SSH Agent**
	- **Install without restart**
- Jenkins > aws-temp-app-code > Credentials > stores scoped for aws-temp-app-code > Global credentials (unrestricted) > Add Credentials
	- Kind: **SSH Username with private key**
	- ID: **ec2-server-key**
	- Username: **ec2-user**
	- Private Key/Enter Directly: **contents of the .pem file**

### Update Jenkinsfile to Connect to EC2 and Run Docker Commands
- Open the Jenkinsfile from our `aws-temp-app-code` project in an editor
- Under the Deploy stage script section we will add this code snippet to test the functionality.
	- Get your EC2_PUBLIC_IP from the AWS EC2 portal
	- We will ignore key checking since this is a non-interactive mode action so we don't get a pop-up
```
script {
	def dockerCmd = 'docker run -d -p 3080:3080 DOCKER_REPO/APP:1.0'
	sshagent(['ec2-server-key']) {
		sh "ssh -o StrictHostKeyChecking=no ec2-user@EC2_PUBLIC_IP ${dockerCmd}"
}
```
- Commit this code back to the repository for now

### Configure the Firewall for EC2 
- AWS > EC2 > Security Groups > security-group-docker-server > Edit Inbound Rules
	- Modify the previous Custom TCP port from 3000 to **3080** for this project
	- Add another SSH option for our **JENKINS_SERVER_IP/32**
	- **Save rules**

### Run Jenkins Pipeline and access Web App
- From the Jenkins server pipeline job, choose **Build Now**

![Successful Jenkins EC2 Deploy](/images/m9-2-successful-app-launch-from-jenkins.png)

### Run an Entire Pipeline
- We will use a new Jenkinsfile that was previously configured for use with a Jenkins Shared Library. A sample copy can be obtained from [this repository](https://gitlab.com/twn-devops-bootcamp/latest/09-aws/java-maven-app/-/blob/starting-code/Jenkinsfile-SharedLibrary).
	- We will utilize all of the code and we will also keep our previous section that included the **sshagent** portion
	- We also need to add our new Dockerfile from the **jenkins-jobs** branch from the above repository
- Once the job runs we should have the same result but with utilizing our Jenkinsfile with a Jenkins Shared Library and Docker like we had before.
	- Note: To access the web app you will need to allow port **8080**.

![Successful App Launch 2](/images/m9-2-successful-app-launch-2.png)
